import React, {PropTypes} from 'react';

class TodoItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = { text: '' }

    // events
    this.updateTodo = () => this.props.updateTodo(this.props.index, this.state.text);
    this.cancelEdit = () => this.props.cancelEdit();
    this.updateText = e => this.setState({
      text: e.target.value
    });
    this.onKeyPress = e => e.charCode == 13 && this.updateTodo();
  }

  render() {
    if (this.props.editing) {
      return (
        <div className='input-group'>
          <input type='text' value={this.state.text} onChange={this.updateText}
            onKeyPress={this.onKeyPress}
            className='form-control' placeholder={this.props.text} />
          <span className='input-group-btn'>
            <button className='btn btn-default' type='button' onClick={this.updateTodo}>Save</button>
            <button className='btn btn-default' type='button' onClick={this.cancelEdit}>Cancel</button>
          </span>
        </div>
      );
    } else {
      return (
        <a href={ "#/edit/" + this.props.index }>
          { this.props.text }
        </a>
      );
    }
  }
}
TodoItem.propTypes = {
  editing: React.PropTypes.bool,
  index: React.PropTypes.number,
  text: React.PropTypes.string.isRequired,
  updateTodo: React.PropTypes.func.isRequired,
  cancelEdit: React.PropTypes.func.isRequired
}

export default TodoItem;
