import React, {PropTypes} from 'react';

const NotFound = () =>
    <div>
        Not found!
    </div>;

export default NotFound;
