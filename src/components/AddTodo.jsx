import React, {PropTypes} from 'react';

require('./AddTodo.scss');

class AddTodo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { todoText: '' };

    // event handlers
    // declare them here, rather than as class methods, so you don't have to use bind in the render method
    // see: https://daveceddia.com/avoid-bind-when-passing-props/
    this.updateTodoText = evt => this.setState({
      todoText: evt.target.value
    });
    this.addTodo = () => this.props.addTodo(this.state.todoText);
    this.onKeyPress = (evt) => {
      if(evt.charCode == 13) {
        this.props.addTodo(this.state.todoText);
      }
    };
  }

  render() {
    return (
      <div className='AddTodo'>
        <div className='input-group'>
          <input className="form-control" type='text' value={this.state.todoText}
            onKeyPress={this.onKeyPress}
            onChange={this.updateTodoText} />
          <span className='input-group-btn'>
            <button className='btn btn-default' onClick={this.addTodo}>Add</button>
          </span>
        </div>
      </div>
    );
  }
}

export default AddTodo;
