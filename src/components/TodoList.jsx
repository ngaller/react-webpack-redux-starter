import React, {PropTypes} from 'react';
import AddTodo from './AddTodo';
import TodoItem from './TodoItem';


const TodoList = ({ todos, editIndex, addTodo, cancelEdit, updateTodo }) => {
  const renderTodo = (todo, index) =>
    <li className='list-group-item' key={index}>
      <TodoItem index={index} editing={editIndex == index} text={todo} cancelEdit={cancelEdit} updateTodo={updateTodo} />
    </li>;
  return (
    <div>
      <AddTodo addTodo={addTodo} />

      <ul className='list-group'>
        { todos.map(renderTodo) }
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired
};

export default TodoList;
