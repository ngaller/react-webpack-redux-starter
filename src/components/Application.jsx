import React from 'react';
import TodoListContainer from '../containers/TodoListContainer';
import NotFound from './NotFound';

export default function Application(props) {
  return (
    <main className="container">
      <h1>The awesome Todo List</h1>

      {selectChildContainer(props)}
    </main>
  );
}

const selectChildContainer = props => {
  const location = props.location;

  switch(location.name) {
    case 'todoList':
      return <TodoListContainer />
    case 'todoEdit':
      return <TodoListContainer editIndex={location.options.id} />;
    default:
      return <NotFound />
  }
};

