import { connect } from 'react-redux';
import Application from '../components/Application';

const mapStateToProps = (state) => {
  return {
    location: state.navigation.location
  };
};

const AppContainer = connect(mapStateToProps)(Application);
export default AppContainer;
