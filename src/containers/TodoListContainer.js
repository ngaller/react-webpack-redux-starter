import { connect } from 'react-redux';
import TodoList from '../components/TodoList';
import * as actions from '../actions/todos';

const TodoListContainer = connect(state => ({
  todos: state.data.todos
}), {
  addTodo: actions.addTodo,
  updateTodo: actions.updateTodo,
  cancelEdit: actions.cancelEdit,
  deleteTodo: actions.deleteTodo
})(TodoList);

export default TodoListContainer;
