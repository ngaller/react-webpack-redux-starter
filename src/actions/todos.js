import { createActions } from 'redux-actions';
import * as navigation from './navigation';

export const { addTodo, deleteTodo } = createActions({
  ADD_TODO: todoText => todoText,
  DELETE_TODO: todoIndex => todoIndex,
});

export const updateTodo = (index, text) => [
  { type: 'UPDATE_TODO', payload: { index, text } },
  navigation.start('todoList')
];
export const cancelEdit = () => navigation.start('todoList');
