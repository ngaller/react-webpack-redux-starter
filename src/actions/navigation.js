import ROUTES from '../constants/ROUTES';

export function start(name, options) {
  const currentURI = window.location.hash.substr(1);
  const newURI = ROUTES.generate(name, options);

  if (currentURI != newURI) {
    window.location.replace(
      window.location.pathname + window.location.search + '#' + newURI
    );
  }
  return {
    type: 'NAVIGATION.START'
  };
}

export function complete() {
  const route = window.location.hash.substr(1);
  return {
    type: 'NAVIGATION.COMPLETE',
    payload: ROUTES.lookup(route)
  };
}
