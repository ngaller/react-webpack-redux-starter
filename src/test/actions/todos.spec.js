import * as actions from '../../actions/todos';

describe('todos actions', function() {
  it('should create an action to add a todo', function() {
    expect(actions.addTodo('Testing')).to.eql({
      type: 'ADD_TODO',
      payload: 'Testing'
    });
  });
});
