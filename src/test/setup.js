import React from 'react'; // eslint-disable-line no-unused-vars
import expect from 'expect.js';
import { jsdom } from 'jsdom';

global.document = jsdom('');
global.window = document.defaultView;
global.navigator = { userAgent: 'browser' };

global.React = React;
global.expect = expect;


