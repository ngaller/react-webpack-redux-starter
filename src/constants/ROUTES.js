import uniloc from 'uniloc';

export default uniloc({
  todoList: 'GET /',
  todoEdit: 'GET /edit/:id'
});
