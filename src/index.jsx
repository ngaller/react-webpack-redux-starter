import ReactDOM from 'react-dom';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import multi from 'redux-multi';

import * as navigation from './actions/navigation';
import rootReducer from './reducers';
import AppContainer from './containers/AppContainer';

require('./app.scss');

const store = createStore(rootReducer, compose(
  applyMiddleware(multi),
  window.devToolsExtension && window.devToolsExtension()
));

// Dispatch navigation events when the URL's hash changes, and when the
// application loads
function onHashChange() {
  store.dispatch(navigation.complete())
}
window.addEventListener('hashchange', onHashChange, false)
onHashChange()

const APP_NODE = document.getElementById('react-app');
ReactDOM.render(
    <Provider store={store}>
        <AppContainer />
    </Provider>,
    APP_NODE);
