const defaultState = {
  transitioning: false,
  location: null
};

export default function(state = defaultState, action) {
  switch (action.type) {
    case 'NAVIGATION.COMPLETE':
      return Object.assign({}, state, {
        transitioning: false,
        location: action.payload
      });
    case 'NAVIGATION.START':
      return Object.assign({}, state, {
        transitioning: true
      });
  }
  return state;
}
