import { combineReducers } from 'redux';

import todoList from './todoList';
import navigation from './navigation';

export default combineReducers({
  data: combineReducers({
    todos: todoList
  }),
  navigation
});
