export default function(state = [], action) {
  let newState;
  switch (action.type) {
    case 'ADD_TODO':
      return state.concat(action.payload);
    case 'UPDATE_TODO':
      newState = state.slice();
      newState[action.payload.index] = action.payload.text;
      return newState;
    case 'DELETE_TODO':
      newState = state.slice();
      newState.splice(action.payload, 1);
      return newState;
  }
  return state;
}
