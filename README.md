I want to put together a starter kit, these are the tools that I am going to include for now:

 * React
 * Redux:
   - redux
   - react-redux
   - redux-promise (middleware for async actions)
   - redux-multi (so we can combine actions, i.e. a "loading" action followed by the one that has the promise)
   - redux-actions (for standard actions)
 * mocha, for testing
   - expect.js, for assertions
   - enzyme, for testing React components
   - (karma) not using that, but it could be if we need to test components in the browser
 * webpack, to bundle stuff
   - uses babel
   - html-webpack-plugin, this will generate the index.html file based on src/index.html
 * sass, more potential than less
 * eslint configuration, use airbnb as base
 * No gulp - webpack does the build, npm runs the tasks

Directory layout:
 * src: Javascript and CSS source files.
    - the CSS files are included via require statements
    - as much as possible each component should require() its own CSS file
 * tests

To build the app, use `npm run build`.
To run it in dev, use `npm start` and connect to localhost:4000 (the port can be changed in package.json, if needed)

The other branches on this repo have some more specific examples:

 * example-with-firebase: simple example of todo list backed by firebase
