var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebPackPlugin = require('html-webpack-plugin');
var pjson = require('./package.json');
var env = require('node-env-file');

// try to load them from a config/development or config/production if present
try {
  env(__dirname + '/config/' + (process.env.NODE_ENV || 'development'));
} catch (e) {
  // don't care
}
// load any undefined ENV variables from .env
// note that variables defined in the environment file will take precedence
// since they are defined first
try {
  env(__dirname + '/.env');
} catch(e) {
  // don't care
}
var DEBUG = process.env.NODE_ENV !== 'production';

var plugins = [
  // shared plugins that need to come first
  new webpack.ProvidePlugin({
    'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
  }),
  new webpack.DefinePlugin({
    // take care not to remove the stringify.
    // these values are not truly defined, they are inlined in the compiled bundle
    // (like a C macro define)
    'process.env': {
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    }
  })
].concat(DEBUG ? [
  // debug only plugins (none at this point)
] : [
  // production only plugins
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      // React lib will have a bunch of warnings
      warnings: false
    }
  }),
  new ExtractTextPlugin('style.css')
]).concat(
  // plugins that are shared between prod / debug builds
  new HtmlWebPackPlugin({
    template: './index.html',
    title: pjson.description
  })
);

var config = {
  entry: DEBUG ? [
    'babel-polyfill',
    'webpack-dev-server/client?' + process.env.REACT_APP_DEV_URL,
    'webpack/hot/only-dev-server',
    './src/index.jsx'
  ] : [
    'babel-polyfill',
    // fetch polyfill
    'whatwg-fetch',
    './src/index.jsx'
  ],
  module: {
    // run eslint for debug builds only
    preLoaders: DEBUG ? [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader"
      },
    ] : [ ],
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel'
    },
      {
        test: /\.scss$/,
        loader: DEBUG ?
          'style!css!sass' :
          // in prod, want to bundle the CSS as a separate file
          ExtractTextPlugin.extract("style-loader", "css-loader?sourceMap!sass?outputStyle=expanded")
      },
      // embed small png images as Data Urls and jpg images as files
      { test: /\.(png|svg)$/, loader: "url-loader?limit=100000" },
      { test: /\.jpg$/, loader: "file-loader" }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist',
    // in development, have to specify the URL for the public path or the images won't load in the CSS due
    // to the fact that the stylesheet gets embedded as a resource (to support the source map)
    // http://stackoverflow.com/questions/34133808/webpack-ots-parsing-error-loading-fonts/34133809#34133809
    publicPath: DEBUG ? process.env.REACT_APP_DEV_URL : '/',
    filename: 'bundle.js'
  },
  debug: DEBUG,
  // this is set by default in watch mode anyway.
  // note that, with this set, eslint will not reparse a module that was already done, so if
  // an error is fixed by editing a different file (eg missing default export) the file that is
  // giving the error itself needs to be resaved
  cache: DEBUG,
  devtool: DEBUG && 'source-map',
  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
  },
  eslint: {
    configFile: './.eslintrc'
  },
  plugins: plugins
};

module.exports = config;
